#include "select-algorithm.hpp"
#include <csignal>

#include <rule-traffic.hpp>
#include <time-measurer.hpp>
#include <dummy-io.hpp>
#include <dummy-update-manager.hpp>
#include <benchmark.hpp>
#include <common.hpp>
#include <ll-bitmap.hpp>
#include <ll-trie.hpp>
#include <pruned-field.hpp>
#include <tss-pr.hpp>
#include <numa-arch-helper.hpp>
#include <ec-tss.hpp>
#include <hc-tss.hpp>

volatile bool globalStopRequested = false;
int globalNumFields = 0;
bool globalDebugFlag = false;

void signalHandler(int signal)
{
  LOG_INFO("signal is processing ...\n");
  globalStopRequested = true;
}

#include "complexity.hpp"
int main(int argc, char** argv)
{
  auto args = pp::parse_command_args(argc, argv);
  signal(SIGINT, signalHandler);

#ifdef __linux__
  nm::init_numa_cpu_env();
#endif
  
  if (args.runGenerator) {
    globalNumFields = args.nFields;
    if(args.type != 2){
      pp::generateDataFile(args.fib.c_str(), args.nRules, args.nFields, args.data_base_dir,
			 args.type, std::bind(pp::selectAlgorithm, args));
    }
    else{
      printf("Convert multi rule to pc rule...\n");
      pp::convertRuleM2P(args.con_src_rule, args.con_dst_dir);
      printf("done!\n");

      printf("Convert multi traffic to pc traffic...\n");
      pp::convertTrafficM2P(args.con_src_traffic, args.con_dst_dir);
      printf("done!\n");
    }
    return 1;
  }
  else if (args.runTest) {
    pp::DummyIOParameters io_params;
    io_params.data_type = pp::DataType::MULTI_FIELD;
    io_params.traffic_file = args.trafficPath;
    io_params.tx_rate = args.txRate;
    io_params.max_io_events = 100000;
    io_params.test_seconds = args.testTime;

    pp::DummyUpdateManagerParameters upd_params;
    upd_params.data_type = pp::DataType::MULTI_FIELD;
    upd_params.update_file = args.updatePath;
    upd_params.insert_rate = args.insRate;
    upd_params.delete_rate = args.delRate;
    upd_params.max_updates = args.maxUpdates;

    pp::BenchmarkParams params;
    params.data_type = pp::DataType::MULTI_FIELD;
    params.rule_file = args.rulePath;
    params.traffic_file = args.trafficPath;
    params.update_file = args.updatePath;
    params.enable_match = args.enableMatch;
    params.enable_insert = args.enableInsert;
    params.enable_delete = args.enableDelete;

    auto readNumFields = [] (const std::string& path) -> int {
      assert(!path.empty());
      FILE* file = fopen(path.c_str(), "r");				
      if (!file) LOG_ERROR("can not open file: %s\n", path.c_str());

      int nFields;
      fscanf(file, "%d", &nFields);
      fclose(file);
    
      return nFields;
    };
    assert(readNumFields(args.rulePath) == readNumFields(args.trafficPath));
    args.nFields = globalNumFields = readNumFields(args.rulePath);

    pp::DummyIO io(io_params);
    pp::Benchmark benchmark(params,
			    (pp::PacketIO*)&io,
			    pp::getDummyUpdateManager(upd_params));
    benchmark.displayConfigurations();
    
    auto pu = selectAlgorithm(args);
    auto statistic = benchmark.run(pu);

    LOG_FILE(args.statisticFile, "| %10s ", args.testName.c_str());
    statistic.dump(args.statisticFile);
    
    if (pu) delete pu;
    if (args.statisticFile) fclose(args.statisticFile);
      
    return 1;
  }

  // globalNumFields = 2;
  // pp::Benchmark::TEST({
  //     .data_type = pp::DataType::MULTI_FIELD,
  //     .rule_file = "test-data/2_8.rule",
  //     .traffic_file = "test-data/2_8_match.traffic",
  //     .result_file = "test-data/2_8_match.traffic.result"	
  // });

  //pp::TimeMeasurer::TEST();
  
  // pp::DummyIO::TEST({
  //   .traffic_file = "test-data/2_8.traffic",
  //   .data_type = pp::DataType::MULTI_FIELD,
  //   .tx_rate = 1, // MPPS
  //   .max_io_events = 10000
  // });

  //LLBitMap::TEST();
  //pp::LLTrie::TEST();
  //pp::PrunedField::TEST();
  //pp::PrunedTupleSpaceSearch::TEST();
  // pp::TupleChain::TEST();
  // pp::Benchmark::TEST({
  //   .data_type = pp::DataType::MULTI_FIELD,
  //   .rule_file     = "../data/rule/2_1000.rule",
  //   .traffic_file  = "../data/traffic/2_1000_match.traffic",
  //   .result_file   = "../data/traffic/2_1000_match.traffic.result",
  //   .profiler_file = NULL,
  // });
  //pp::ChainedTupleSpaceSearch::TEST();
  //pp::ExChainedTupleSpaceSearch::TEST();
  //pp::HeadChainedTupleSpaceSearch::TEST();
  com::TEST(argc, argv);
  return 0;
}
