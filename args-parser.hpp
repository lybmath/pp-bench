#define _GUN_SOURCE
#include <libgen.h>//dirname
#include <string.h>//strstr
#include <unistd.h>
#include <string>
#include <vector>
#include <stdio.h>
#include <logger.hpp>
#include <rule-traffic.hpp>

namespace pp {

enum TestOptions {
  DISABLE_MATCH = 0x1,
  DISABLE_INSERT = 0x2,
  DISABLE_DELETE = 0x4,
  RANDOM_TRAFFIC = 0x8
};
 
struct CommandArgs
{
  bool runGenerator;
  bool runTest;

  // Generator
  int type;
  int nFields, nRules;
  std::string fib;
  std::string data_base_dir;
  char * con_src_rule;
  char * con_src_traffic;
  char * con_dst_dir;
  
  // benchmark
  std::string testName;
  std::string rulePath;
  std::string trafficPath;
  std::string updatePath;
  bool enableMatch;
  bool enableInsert;
  bool enableDelete;
  bool useRandomTraffic;
  FILE* statisticFile;
  int maxUpdates;
  int testTime; // seconds
  double txRate; // MPPS
  double insRate; // KUPS
  double delRate; // KUPS

  // algorithm
  std::string algorithmName;
  int depth;
  int threshold;
  int treeHeight;
  uint32_t maskThreshold;

  CommandArgs()
    : runGenerator(false)
    , runTest(false)
    , type(0)
    , nFields(2)
    , nRules(0)
    , enableMatch(true)
    , enableInsert(true)
    , enableDelete(true)
    , useRandomTraffic(false)
    , statisticFile(NULL)
    , maxUpdates(10000)
    , testTime(2)
    , txRate(1)
    , insRate(0)
    , delRate(0)
    , treeHeight(4)
    , depth(1)
    , threshold(0)
    , maskThreshold(0)
  {}
};

void
print_usage(char** argv)
{
  LOG_INFO("##########################################\n"
	   "./%s -[apdtfGfnsTnrutxids]\n"
	   "  -G: run generator\n"
     "\t-t: type , 0 for multi_field, 1 for packet classification, 2 for convert mode\n"
	   "\t-f: # of fileds\n"
	   "\t-n: # of rules (packets)\n"
	   "\t-s: name of origin file, in convert mode, file#1 is rule file, file#2 is traffic file\n"
     "\t-d: dst dir for convert mode"
	   "  -T: run test\n"
	   "\t-n: rule name under test-data/\n"
	   "\t-r: rule name under data/rule/\n"
	   "\t-o: test options\n"
	   "\t-u: max number of updates\n"
	   "\t-t: max test time in seconds\n"
	   "\t-x: tx rate of the emulator in MPPS\n"
	   "\t-i: ins rate of the emulator in KUPS\n"
	   "\t-d: del rate of the emulator in KUPS\n"
	   "\t-s: statistic file and open option, separated by [:]\n"
	   "  -a: name of the algorithm to benchmark\n"
	   "  -m: mask threshold\n"
	   "  -d: depth for ec-tss\n"
	   "  -t: threshold for ec-tss\n"
	   "  -h: maximal tree height for tss-pr\n", argv[0]);
}

std::vector<std::string>
splitStrings(std::string input, const std::string& sep = ",")
{
  //LOG_INFO("to sep %s by %s\n", input.c_str(), sep.c_str());
  std::vector<std::string> res;
  size_t pos = 0;
  while ((pos = input.find(sep, pos)) != std::string::npos) {
    res.push_back(input.substr(0, pos));
    input = input.substr(pos + 1);
  }
  res.push_back(input);
  //for (auto& str : res) LOG_INFO("%s\n", str.c_str());
  return res;
}

CommandArgs
rt_parse_command_args(int argc, char** argv, CommandArgs& args)
{
  args.runGenerator = true;
  
  int opt;
  int data_nb, field_nb;
  char filepath[256]="";
  char app_abs_path[256]="";
  char data_base_dir[256]="";

  auto parse_multi_field = [&](){  
    while( (opt = getopt(argc, argv, "f:n:s:")) != -1){
      switch(opt){
        case 'f':
          args.nFields = atoi(optarg);
          break;
        case 'n':
          args.nRules = atoi(optarg);
          break;
        case 's':
          args.fib = std::string(optarg);
          break;
        default:
          print_usage(argv);
          exit(1);
      }
    }
  };

  auto parse_packet_classification = [&](){
    args.nFields = 5; // 5 tuple class bench rule
    while( (opt = getopt(argc, argv, "s:")) != -1){
      switch(opt){
        case 's':
          args.fib = std::string(optarg);
          break;
        default:
          print_usage(argv);
          exit(1);
      }
    }
  };

  auto convert_multi_to_pc = [&](){
    char * dst_dir;
    char * rule_file = NULL;
    char * traffic_file = NULL;

    args.nFields = 5;
    while( (opt = getopt(argc, argv, "s:d:")) != -1){
      switch(opt){
        case 's':
          args.con_src_rule = argv[optind-1];
          args.con_src_traffic = argv[optind];
          break;
        case 'd':
          args.con_dst_dir = optarg;
          break;
        default:
          print_usage(argv);
          exit(1);
      }
    }
  };

  // calculate the current absolute data dir
  getcwd(app_abs_path, 256);
  if(strstr(app_abs_path, "build") != NULL){
    sprintf(data_base_dir, "%s/data",dirname(app_abs_path) );
  }
  else{
    sprintf(data_base_dir, "%s/data", app_abs_path);
  }
  args.data_base_dir = std::string(data_base_dir);

  while((opt = getopt(argc, argv, "t:fns")) != -1){
    switch(opt){
      case 't':
        if(atoi(optarg) == 0){
          args.type = 0;
          parse_multi_field();
        }
        else if(atoi(optarg) == 1){
          args.type = 1;
          parse_packet_classification();
        }
        else{
          args.type = 2;
          convert_multi_to_pc();
        }
        break;
      case 'f':
      case 'n':
      case 's':
        printf("Set data mode first \"-t\"\n");
        exit(1);
      default:
        print_usage(argv);
        exit(1);
    }
  }
	


  return args;
}

CommandArgs
test_parse_command_args(int argc, char** argv, CommandArgs& args)
{
  args.runTest = true;

  int opt;
  std::string ruleDir = std::string("data/rule/");
  std::string trafficDir = std::string("data/traffic/");
  std::string updateDir = std::string("data/update/");
  std::vector<std::string> splitResult;
  int options;

  while( (opt = getopt(argc, argv, "n:r:x:i:d:u:t:s:o:")) != -1){
    switch(opt){
    case 'n':
      ruleDir = trafficDir = updateDir = std::string("test-data/");
    case 'r':
      args.testName = optarg;
      args.rulePath = ruleDir + optarg + std::string(".rule");
      args.trafficPath = trafficDir + optarg + std::string("_match.traffic");
      args.updatePath = updateDir + optarg + std::string(".update");
      break;
    case 'o':
      options = atoi(optarg);
      if (options & TestOptions::DISABLE_MATCH) args.enableMatch = false;
      if (options & TestOptions::DISABLE_INSERT) args.enableInsert = false;
      if (options & TestOptions::DISABLE_DELETE) args.enableDelete = false;
      if (options & TestOptions::RANDOM_TRAFFIC) args.useRandomTraffic = true;
      break;
    case 'x':
      args.txRate = atof(optarg);
      break;
    case 'i':
      args.insRate = atof(optarg);
      break;
    case 'd':
      args.delRate = atof(optarg);
      break;
    case 'u':
      args.maxUpdates = atoi(optarg);
      break;
    case 't':
      args.testTime = atoi(optarg);
      break;
    case 's':
      splitResult = splitStrings(optarg, ":");
      if (splitResult.size() == 2) {
	args.statisticFile = fopen(splitResult[0].c_str(), splitResult[1].c_str());
      }
      else if (splitResult.size() == 1) {
	args.statisticFile = fopen(splitResult[0].c_str(), "w");
      }
      break;
    default:
      print_usage(argv);
      exit(1);
    }
  }

  if (args.useRandomTraffic) {
    args.testName += "_R";
    auto nPos = args.trafficPath.find("_match.traffic");
    if (nPos != std::string::npos) {
      args.trafficPath.replace(nPos, std::string::npos, "_random.traffic");
    }
  }
  return args;
}

CommandArgs
parse_command_args(int argc, char** argv)
{
  struct CommandArgs args;

  int c;
  while ((c = getopt (argc, argv, "GTa:m:d:t:h:")) != -1) {
    switch (c) {
    case 'G': rt_parse_command_args(argc, argv, args); break;
    case 'T': test_parse_command_args(argc, argv, args); break;
    case 'a': args.algorithmName = optarg; break;
    case 'm': sscanf(optarg, "%x", &args.maskThreshold); break;
    case 'd': args.depth = atoi(optarg); break;
    case 't': args.threshold = atoi(optarg); break;
    case 'h': args.treeHeight = atoi(optarg); break;
    default: print_usage(argv); exit(1);
    }
  }

  return args;
}

} // namespace pp
