#include <pruned-field.hpp>
#include <common.hpp>

namespace pp {

PrunedField::PrunedField(const int& maxTuples, int maxLength,
			 ProcessingUnit::Profiler* const& profiler)
  : m_profiler(profiler)
  , m_maxTuples(maxTuples)
  , m_tree(new LLTrie(std::bind(&PrunedField::push, this, _1, _2), maxLength, profiler))
  , m_maps(1, new LLBitMap(m_maxTuples))
{
}
  
PrunedField::~PrunedField()
{
  for (int i = 0; i < m_maps.size(); ++i) {
    if (m_maps[i]) delete m_maps[i];
    m_maps[i] = nullptr;
  }

  if (m_tree) {
    delete m_tree;
    m_tree = nullptr;
  }
}

trie_value_t
PrunedField::push(const trie_value_t& from, const trie_value_t& to)
{
#define MERGE_TRIE_VALUE(m, x) \
  x.s.type == TRIE_CUSTOM_VALUE_TYPE ? m.setBit(x.s.info) : m |= *m_maps[x.s.info]

  m_maps.push_back(new LLBitMap(m_maxTuples));
  auto& map = *m_maps.back();
  
  MERGE_TRIE_VALUE(map, from);
  MERGE_TRIE_VALUE(map, to);

  return m_maps.size() - 1;
}

void
PrunedField::insertConstruction(const uint32_t& field, const uint32_t& mask, int tupleId)
{
  m_tree->insertConstruction(field, __builtin_popcount(mask),
			    trie_value_t(tupleId, TRIE_CUSTOM_VALUE_TYPE));
}

void
PrunedField::insertConstruction(const uint32_t& field, const uint8_t& len, int tupleId)
{
  m_tree->insertConstruction(field, len, trie_value_t(tupleId, TRIE_CUSTOM_VALUE_TYPE));
}

uint64_t
PrunedField::finalizeConstruction(int height)
{
  auto res = m_tree->reconstruction(height); // field trie
  res += m_maps.size() * ((m_maxTuples + 7) >> 3); // bitmaps 
  return res;
}

void
PrunedField::insert(const uint32_t& field, const uint32_t& mask, int tupleId)
{
  m_tree->insert(field, __builtin_popcount(mask), trie_value_t(tupleId, TRIE_CUSTOM_VALUE_TYPE));
}

bool
PrunedField::matchField(const uint32_t& field, LLBitMap* const &res)
{
  auto value = m_tree->longestPrefixMatch(field);
  if (value.s.type == TRIE_CUSTOM_VALUE_TYPE) {
    return res->intersect(value.s.info);
  }
  return value.s.info != 0 && res->intersect(*m_maps[value.s.info]);
}

void
PrunedField::display()
{
  m_tree->display();
  LOG_DEBUG("-----------------------------------\n");
  for (int i = 0; i < m_maps.size(); ++ i) {
    LOG_DEBUG("[%3d] %p: ", i, m_maps[i]);
    m_maps[i]->print();
  }
  LOG_DEBUG("-----------------------------------\n");
}
  
} // namespace pp

void
pp::PrunedField::TEST()
{
  auto test = [] (const std::vector<trie_key_t>& key, int height) {
    int nTuples = 8;
    PrunedField pField(nTuples, 5);
    for (int i = 0; i < key.size(); ++ i) {
      pField.insertConstruction(key[i].addr, key[i].len, i);
    }
    pField.finalizeConstruction(height);

    LLBitMap res(nTuples);
    for (int i = 0; i < key.size(); ++ i) {
      res.setAllBits();

      LOG_DEBUG("%08x --> ", key[i].addr);
      if (pField.matchField(key[i].addr, &res)) {
	res.print();
      }
      else {
	LOG_DEBUG(" FAILE\n");
      }
    }
  };

  std::vector<trie_key_t> keys = {
    {0xacffffff, 1}, {0xacffffff, 2}, {0xacffffff, 5}, {0x88ffffff, 5},
    {0x00ffffff, 1}, {0xacffffff, 5}, {0xacffffff, 4}, {0xacffffff, 4}
  };
  test(keys, -4);
}
