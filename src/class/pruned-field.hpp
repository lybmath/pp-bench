#ifndef PRUNED_FIELD_HPP
#define PRUNED_FIELD_HPP

#include <ll-trie.hpp>
#include <ll-bitmap.hpp>
#include <processing-unit.hpp>

namespace pp {
  
class PrunedField
{
public:
  static void TEST();
  
public:
  PrunedField(const int& maxTuples, int maxLength = 32,
	      ProcessingUnit::Profiler* const& profiler = nullptr);
  ~PrunedField();

  trie_value_t
  push(const trie_value_t& from, const trie_value_t& to);

  void
  insertConstruction(const uint32_t& field, const uint32_t& mask, int tupleId);

  void
  insertConstruction(const uint32_t& field, const uint8_t& len, int tupleId);

  void
  insert(const uint32_t& field, const uint32_t& mask, int tupleId);

  uint64_t
  finalizeConstruction(int height);

  bool
  matchField(const uint32_t& field, LLBitMap* const &res);

  void
  display();
  
private:
  const int& m_maxTuples;
  ProcessingUnit::Profiler* const &m_profiler;
  LLTrie* m_tree;
  std::vector<LLBitMap*> m_maps;
};

} // namespace pp

#endif // PRUNED_FIELD_HPP
