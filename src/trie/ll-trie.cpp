#include <ll-trie.hpp>
#include <ll-bitmap.hpp>
#include <common.hpp>
#include <tss-pr.hpp>
#include <list>

namespace pp {

#define CHECK_BITS(k, p, n) (((k) << p) >> (32 - (n)))

bool operator == (const trie_value_t& x, const trie_value_t& y)
{
  return x.bytes == y.bytes;
}
bool operator != (const trie_value_t& x, const trie_value_t& y)
{
  return x.bytes != y.bytes;
}

LLTrie::LLTrie(const trie_value_push_func& push, int length,
		 ProcessingUnit::Profiler* const& profiler)
  : m_pushFunc(push)
  , m_maxPrefixLength(length)
  , m_treeRoot(new trie_node_t)
  , m_insertRangeQ(nullptr)
  , m_profiler(profiler)
{
}

LLTrie::~LLTrie()
{
  LLTrie::travelB(m_treeRoot, [] (trie_node_t* node) {
      if (node) {
	delete node;
	node = nullptr;
      }
    });

  MEM_FREE(m_insertRangeQ);
}

void
LLTrie::insertConstruction(const trie_key_t& key,
			   const trie_value_t& value)
{
  insertConstruction(key.addr, key.len, value);
}

void
LLTrie::insertConstruction(const uint32_t& addr, int len,
			   const trie_value_t& value)
{
  auto curr = m_treeRoot;

  for (int i = 0 ; i < len ; ++i) {
    if (CHECK_BITS(addr, i, 1) == 0) {
      if (curr->left == nullptr) {
        curr->left = new trie_node_t(curr->level + 1);
      }
      curr = curr->left;
    } else {
      if (curr->right == nullptr) {
        curr->right = new trie_node_t(curr->level + 1);
      }
      curr = curr->right;
    }
  }

  curr->value = push(value, curr->value);
}

void
LLTrie::insert(uint32_t address, int len, trie_value_t value)
{
  int sid = 0, uid = 0, stride = m_strides[0];
  while (len > stride) {
    uid += CHECK_BITS(address, 0, stride);
    address <<= stride; len -= stride;
    stride = m_strides[++ sid]; // get next stride

    if (m_units[uid].s.type != TRIE_POINTER) {
      m_units.insert(m_units.end(), 1u << stride, m_units[uid]); // insert 2^stride new units
      value = push(value, m_units[uid]); // merge info, carried in [value] and pushed down
       m_units[uid] = trie_value_t(m_units.size() - (1u << stride), TRIE_POINTER); // overwrite [unit]
    }

    uid = m_units[uid].s.info; // when gets there, it must be a trie pointer
  }

  m_insertRangeQ[0]
    = trie_range_t(uid + (CHECK_BITS(address, 0, len) << (stride - len)), stride - len, sid);
  for(int qid = 0; qid >= 0; qid --) {
    auto range = m_insertRangeQ[qid];
    for (int i = range.start; i < range.end; ++ i) {
      if (m_units[i].s.type != TRIE_POINTER) {
	m_units[i] = push(value, m_units[i]);
      }
      else {
	m_insertRangeQ[qid ++] = trie_range_t(m_units[i].s.info, m_strides[range.sid], range.sid);
      }
    }
  }
}

uint64_t
LLTrie::reconstruction(int height)
{
  LOG_STEP(push(m_treeRoot, 0), "leaf pushing");
  LOG_STEP(LLTrie::calOptStrides(m_treeRoot, m_strides, height, m_maxPrefixLength), "cal strides");

  m_units = std::vector<trie_value_t>(1 << m_strides[0], 0);
  LOG_STEP(transNode(m_treeRoot, 0, m_strides[0], 0), "trans to multibit");

  INIT_PROFILER_C(profiler, PrunedTupleSpaceSearch);
  USE_PROFILER_C(profiler, {
      profiler->nTreeNodes ++; // root
      for (auto& unit : m_units) {
	if (unit.s.type == TRIE_POINTER) {
	  profiler->nTreeNodes ++;
	}
      }
    });

  m_insertRangeQ = MEM_ALLOC(m_units.size(), trie_range_t);
  assert(m_insertRangeQ != nullptr);

  auto res = m_units.size() * 4; // trie units
  res += m_strides.size() * 4; // trie strides
  return res;
}

trie_value_t
LLTrie::longestPrefixMatch(uint32_t address)
{
  INIT_PROFILER_C(profiler, PrunedTupleSpaceSearch);
  int idx = 0;
  trie_value_t unit;
  for (auto&& stride : m_strides) {
    unit = m_units[idx + CHECK_BITS(address, 0, stride)];
    USE_PROFILER(profiler, {profiler->accessedTreeNodes ++;});

    if (unit.s.type != TRIE_POINTER) {
      break;
    }
    idx = unit.s.info;
    address <<= stride;
  }
  return unit;
}

void
LLTrie::push(trie_node_t* node,
	     const trie_value_t& value)
{
  node->value = push(value, node->value);
  if (node->left == nullptr && node->right == nullptr) {
    // no further pushing is needed
    return;
  }
  else if (node->left == nullptr) {
    // create left
    node->left = new trie_node_t(node->level + 1);
  }
  else if (node->right == nullptr) {
    // create right
    node->right = new trie_node_t(node->level + 1);
  }

  push(node->left, node->value);
  push(node->right, node->value);
}

trie_value_t
LLTrie::push(const trie_value_t& from, const trie_value_t& to)
{
  if (to.bytes == 0) return from;
  if (from.bytes == 0) return to;

  uint64_t key = from.bytes;
  key = (key << 32) | to.bytes;

  auto res = m_pushMap.emplace(key, 0);
  if (res.second) {
    res.first->second = m_pushFunc(from, to);
  }

  return res.first->second;
}

void
LLTrie::transNode(trie_node_t* node, int start, int rest, int sid)
{
  if (node->left == nullptr && node->right == nullptr) {
      int nUnits = 1 << rest;
      for (int i = 0 ; i < nUnits ; i ++) {
        m_units[start + i] = node->value;
      }
  }
  else {
    if (rest == 0) {
      LOG_ASSERT(sid + 1 < m_strides.size(), "exceeded stride: %d %lu\n", sid, m_strides.size())

      m_units[start] = trie_value_t(m_units.size(), TRIE_POINTER);
      start = m_units[start].s.info;
      rest = m_strides[++ sid];
      m_units.insert(m_units.end(), 1 << rest, 0);
    }

    if (node->left != nullptr) {
      transNode(node->left, start, rest - 1, sid);
    }
    if (node->right != nullptr) {
      transNode(node->right, start + (1 << (rest - 1)), rest - 1, sid);
    }
  }
}

void
LLTrie::display()
{
  LLTrie::travelF(m_treeRoot, [] (trie_node_t* node) {
      LOG_DEBUG("%p: %3d [%u, %u], (%p, %p)\n",
		node, node->level, node->value.s.type, node->value.s.info, node->left, node->right);
    });

  for (int i = 0; i < m_units.size(); ++ i) {
    LOG_DEBUG("[%3d]: (%2u, %u)\n", i, m_units[i].s.type, m_units[i].s.info);
  }
}

} // namespace pp

void
pp::LLTrie::TEST()
{
  std::vector<trie_key_t> keys = {
    {0xacffffff, 1}, {0xacffffff, 2}, {0xacffffff, 5}, {0x88ffffff, 5},
    {0x00ffffff, 1}, {0xacffffff, 5}, {0xacffffff, 4}, {0xacffffff, 4}
  };
  std::vector<trie_key_t> insertions = {
    {0xacffffff, 4}, {0xacffffff, 5}, {0xacffffff, 5}, {0x2fffffff, 4}
  };

  const int mapSize = keys.size() + insertions.size();
  std::vector<LLBitMap*> maps;
  maps.push_back(new LLBitMap(mapSize));
  trie_value_push_func push = [&] (const pp::trie_value_t& from,
				   const pp::trie_value_t& to) -> pp::trie_value_t {
#define MERGE_TRIE_VALUE(m, x) \
    x.s.type == TRIE_CUSTOM_VALUE_TYPE ? m.setBit(x.s.info) : m |= *maps[x.s.info]

    maps.push_back(new LLBitMap(mapSize));
    auto& map = *maps.back();

    MERGE_TRIE_VALUE(map, from);
    MERGE_TRIE_VALUE(map, to);
    return maps.size() - 1;
  };
  LLTrie tree(push, 5);

  for (int i = 0; i < keys.size(); ++i) {
    tree.insertConstruction(keys[i], pp::trie_value_t(i, pp::TRIE_CUSTOM_VALUE_TYPE));
  }
  tree.reconstruction(-4);

  for (auto&& key : keys) {
    auto res = tree.longestPrefixMatch(key.addr);
    LOG_DEBUG("%08x -->", key.addr);
    if (res.s.type == pp::TRIE_CUSTOM_VALUE_TYPE) {
      LOG_DEBUG("[%u]\n", res.s.info);
    }
    else {
      maps[res.s.info]->print();
    }
  }

  for (int i = 0; i < insertions.size(); ++ i) {
    auto value = pp::trie_value_t(i + keys.size(), pp::TRIE_CUSTOM_VALUE_TYPE);
    tree.insert(insertions[i].addr, insertions[i].len, value);
  }

#ifdef _DEBUG_TRIE_
  tree.display();
  LOG_DEBUG("-----------------------------------\n");
  for (int i = 0; i < maps.size(); ++ i) {
    LOG_DEBUG("[%3d]: ", i);
    maps[i]->print();
  }
#endif

  for (auto& map : maps) delete map;
}
