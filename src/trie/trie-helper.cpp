#include <ll-trie.hpp>
#include <common.hpp>
#include <limits>

namespace pp {

void
LLTrie::travelF(trie_node_t* node, const trie_node_traversal_func& func)
{
  func(node);
  
  if (node->left != nullptr) {
    travelF(node->left, func);
  }

  if (node->right != nullptr) {
    travelF(node->right, func);
  }
}

void
LLTrie::travelM(trie_node_t* node, const trie_node_traversal_func& func)
{
  if (node->left != nullptr) {
    travelM(node->left, func);
  }

  func(node);

  if (node->right != nullptr) {
    travelM(node->right, func);
  }
}

void
LLTrie::travelB(trie_node_t* node, const trie_node_traversal_func& func)
{
  if (node->left != nullptr) {
    travelB(node->left, func);
  }

  if (node->right != nullptr) {
    travelB(node->right, func);
  }

  func(node);
}

uint64_t
LLTrie::calOptStrides(trie_node_t* root, std::vector<int>& strides, int height, int length)
{
  //#define _DEBUG_
  
  const int maxLevel = 128;
  const uint64_t inf = std::numeric_limits<uint64_t>::max();
  LOG_ASSERT(length < maxLevel, "length is too long\n");

  int levelNodes[maxLevel] = {0};
  travelF(root, [&] (trie_node_t* node) {
      if ((node->left != nullptr) || (node->right != nullptr)) {
        levelNodes[node->level] ++;
      }
    });

  int absHeight = height > 0 ? height : -height;
  if (absHeight > length) absHeight = length;
  if (height > 0) {
    LOG_DEBUG("from %d levels to %d levels\n", length, absHeight);
  }
  else {
    LOG_DEBUG("from %d levels to maximal %d levels\n", length, absHeight);
  }
  
  
#ifdef _DEBUG_TRIE_
  for (int i = 0 ; i <= length ; i ++) {
    LOG_DEBUG("%2d: %d\n", i, levelNodes[i]);
  }
#endif

  uint64_t optUnits[maxLevel + 1][maxLevel + 1];
  uint64_t powUnits[maxLevel + 1][maxLevel + 1];
  
  for (int i = 0 ; i <= length ; i ++) {
    for (int j = 0 ; j <= length ; j ++) {
      optUnits[i][j] = inf;
    }
  }
  
  for (int i = 0 ; i <= length ; i ++) {
    for (int j = i ; j <= length ; j ++) {
      powUnits[i][j] = 1;
      powUnits[i][j] <<= (j - i);
    }
  }

  optUnits[0][0] = 0;
  for (int i = 1 ; i <= absHeight ; i ++) {
    for (int j = i ; j <= length ; j ++) {
      optUnits[i][j] = inf;
      for (int k = i - 1 ; k < j ; k ++) {
        if (optUnits[i - 1][k] < inf) {
          uint64_t curr = optUnits[i - 1][k] + powUnits[k][j] * levelNodes[k];
          if (curr < optUnits[i][j]) {
            optUnits[i][j] = curr;
          }
        }
      }
    }
  }

#ifdef _DEBUG_TRIE_
  for (int i = 0; i <= length; ++i) {
    for (int j = 0; j <= length; ++j) {
      if (optUnits[i][j] < inf) {
	LOG_DEBUG("%3llu ", optUnits[i][j]);
      }
      else {
	LOG_DEBUG("inf ");
      }
    }
    LOG_DEBUG("\n");
  }
#endif

  // cal opt height
  int optHeight = absHeight;
  if (height < 0) {
    uint64_t minUnits = inf;
    for (int i = 1; i <= absHeight; ++i) {
      if (optUnits[i][length] < minUnits) {
	minUnits = optUnits[i][length];
	optHeight = i;
      }
    }
  }
  
  strides.clear();
  int Jopt = length;
  for (int i = optHeight ; i > 0 ; i --) {
    for (int k = i - 1 ; k < Jopt ; k ++) {
      if (optUnits[i - 1][k] < inf) {
        uint64_t curr = optUnits[i - 1][k] + powUnits[k][Jopt] * levelNodes[k];
        if (optUnits[i][Jopt] == curr) {
          //printf ("%d %d %d: %d %d\n", i, Jopt, optUnits[i][Jopt], optUnits[i - 1][k], k);
          strides.insert(strides.begin(), Jopt - k);
          Jopt = k;
          break;
        }
      }
    }
  }

  //printf ("%d\n", optUnits[height][32]);
  int totalStrides = 0;
  for (auto&& s : strides) {
    totalStrides += s;
  }
  LOG_ASSERT(totalStrides == length, "invalid strides array!");

  return optUnits[optHeight][length];
}

} // namespace pp
