#ifndef LL_TRIE_HPP
#define LL_TRIE_HPP

#include <cinttypes>
#include <vector>
#include <functional>
#include <spp.h>
#include <unordered_map>
#include <processing-unit.hpp>

namespace pp {
  
struct trie_key_t {
  uint32_t addr;
  uint8_t  len;
};

enum {
  TRIE_INFO = 0,
  TRIE_POINTER = 1,
  TRIE_CUSTOM_VALUE_TYPE
};

union trie_value_t {
  struct {
    uint32_t type : 4;
    uint32_t info : 28;
  } s;
  uint32_t bytes;
  trie_value_t(uint32_t info = 0, uint32_t type = TRIE_INFO) : s({type, info}) {};
};
bool operator == (const trie_value_t& x, const trie_value_t& y);
bool operator != (const trie_value_t& x, const trie_value_t& y);

struct trie_node_t
{
  struct trie_node_t* left;
  struct trie_node_t* right;
  trie_value_t value;
  int level;
  trie_node_t(int l = 0) : left(nullptr), right(nullptr), value(0), level(l) {};
};

struct trie_range_t
{
  int start;
  int end;
  int sid;
  trie_range_t(int s, int l, int n) : start(s), end(s + (1u << l)), sid(n + 1) {};
};

class LLTrie
{
public:
  static void TEST();

public:
  typedef std::function<void(trie_node_t*)> trie_node_traversal_func;
  typedef std::function<trie_value_t(const trie_value_t&,
				     const trie_value_t&)> trie_value_push_func;
  
  static void
  travelF(trie_node_t* node, const trie_node_traversal_func& func);
  
  static void
  travelM(trie_node_t* node, const trie_node_traversal_func& func);
  
  static void
  travelB(trie_node_t* node, const trie_node_traversal_func& func);
  
  static uint64_t
  calOptStrides(trie_node_t* root,
		std::vector<int>& strides,
		int height,
		int length);
  
public:
  LLTrie(const trie_value_push_func& push, int maxPrefixLength = 32,
	 ProcessingUnit::Profiler* const& profiler = nullptr);
  ~LLTrie();

public:
  void
  insertConstruction(const trie_key_t& key,
		     const trie_value_t& value);

  void
  insertConstruction(const uint32_t& addr, int len,
		     const trie_value_t& value);

  void
  insert(uint32_t address, int len, trie_value_t value);

  uint64_t
  reconstruction(int height = 4);

  trie_value_t
  longestPrefixMatch(uint32_t address);

  void
  display();

protected:
  void
  push(trie_node_t* node, const trie_value_t& value);

  trie_value_t
  push(const trie_value_t& from, const trie_value_t& to);

  void
  transNode(trie_node_t* node, int start, int rest, int sid);

private:
  ProcessingUnit::Profiler* const &m_profiler;
  int m_maxPrefixLength;
  trie_node_t* m_treeRoot;
  trie_value_push_func m_pushFunc;

  std::vector<int> m_strides;
  std::vector<trie_value_t> m_units;
  std::unordered_map<uint64_t, trie_value_t> m_pushMap;

  trie_range_t* m_insertRangeQ;
};

  //#define _DEBUG_TRIE_

} // namespace pp

#endif // LL_TRIE_HPP
