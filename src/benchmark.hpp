#ifndef BENCHMARK_HPP
#define BENCHMARK_HPP

#include <time-measurer.hpp>
#include <packet-io.hpp>
#include <update-manager.hpp>
#include <processing-unit.hpp>
#include <string>
#include <cstdio>

namespace pp {

struct BenchmarkParams {
  int data_type;
  std::string rule_file;
  std::string traffic_file;
  std::string update_file;
  bool enable_match;
  bool enable_insert;
  bool enable_delete;
};

struct BenchmarkStatistic {
  std::string algoName;
  uint64_t totalBytes;
  uint64_t totalRules;
  uint64_t constructTime; // ms

  double avgMatchSpeed;
  double avgInsertSpeed;
  double avgDeleteSpeed;
  ProcessingUnit::Profiler* profiler;

  IOStatistic ioStatistic;
  UpdateStatistic updStatistic;

  BenchmarkStatistic(const std::string& name, int nRules, int nPackets)
    : algoName(name)
    , totalBytes(0)
    , totalRules(nRules)
    , constructTime(0)
    , avgMatchSpeed(0)
    , avgInsertSpeed(0)
    , avgDeleteSpeed(0)
    , profiler(nullptr)
  {}

  void
  dump(FILE* statisticFile = NULL);
};
  
class Benchmark
{
public:
  static void TEST(BenchmarkParams params);
  Benchmark(BenchmarkParams params, PacketIO* io, UpdateManager* um = NULL);
  ~Benchmark();

  Benchmark&
  reload(BenchmarkParams params);

  Benchmark&
  reset(PacketIO* io, UpdateManager* um = NULL);

  BenchmarkStatistic
  run(ProcessingUnit* pu);

public:
  Benchmark&
  displayConfigurations();
  
  void
  matchSimulation(ProcessingUnit* pu, BenchmarkStatistic& statistic);

  void
  updateSimulation(ProcessingUnit* pu, BenchmarkStatistic& statistic);
    
  void
  emulation(ProcessingUnit* pu, BenchmarkStatistic& statistic);

protected:
  void
  loadConfigurations(BenchmarkParams params);

private:
  BenchmarkParams m_configs;
  TimeMeasurer m_measurer;
  
  PacketIO* m_io;
  UpdateManager* m_um;
  
  rule_t* m_rules; // to construction pu
  packet_t* m_packets; // for simulation
  action_t* m_actions;
  update_t* m_insertions;
  update_t* m_deletions;
  packet_t* m_updatePackets;
  action_t* m_updateActions;

  int m_nRules;
  int m_nPackets;
  int m_nInsertions;
  int m_nDeletions;
  int m_nUpdatePackets;
};

} // namespace pp

#endif // BENCHMARK_HPP
