#include <rule-traffic.hpp>
#include <common.hpp>

std::tuple<int, int>
loadMultiFieldUpdates(FILE* file, pp::update_t*& insertions, pp::update_t*& deletions)
{
  // line-1: nFileds nInsertions nDeletions
  // following nInsertions + nDeletions lines, each includes [type] [priority] [action] {[field]}
  
  int nFields, nInsertions, nDeletions;
  assert(fscanf(file, "%d %d %d", &nFields, &nInsertions, &nDeletions) == 3);

  insertions = MEM_ALLOC(nInsertions, pp::update_t);
  deletions = MEM_ALLOC(nDeletions, pp::update_t);
  assert(insertions != nullptr && deletions != nullptr);

  int ii = 0, dd = 0;
  int type;
  for (int k = nInsertions + nDeletions; k > 0; -- k) {
    auto rule = MEM_ALLOC(1, pp::pc_rule);
    assert(rule && fscanf(file, "%d", &type) == 1);
    
    rule->nFields = nFields;
    rule->fields = MEM_ALLOC(nFields, uint32_t);
    rule->masks = MEM_ALLOC(nFields, uint32_t);
    assert(rule->fields != NULL && rule->masks != NULL);
    
    assert(fscanf(file, "%d", &rule->priority) == 1);
    assert(fscanf(file, "%u", &rule->action) == 1);
    uint32_t field;
    uint8_t length;
    for (int j = 0; j < nFields; ++ j) {
      assert(fscanf(file, "%x/%hhu", &field, &length) == 2);
      rule->masks[j] = pp::MASK_LEN_32[length];
      rule->fields[j] = field & rule->masks[j];
    }
    
    if (type == pp::UpdateType::INSERT) {
      assert(ii < nInsertions);
      insertions[ii].type = type;
      insertions[ii].rule = (pp::rule_t)rule;
      ii ++;
    }
    else {
      assert(dd < nDeletions);
      deletions[dd].type = type;
      deletions[dd].rule = (pp::rule_t)rule;
      dd ++;
    }
  }

  return std::make_tuple(nInsertions, nDeletions);
}

std::tuple<int, int>
loadClassBenchUpdates(FILE* file, pp::update_t*& insertions, pp::update_t*& deletions)
{
  // TODO.
  return std::make_tuple(0, 0);
}

namespace pp {

std::tuple<int, int>
loadUpdatesFromFile(const char* path, update_t*& inertions, update_t*& deletions, int type)
{
  SAFE_OPEN_FILE(file, path);

  switch(type) {
  case DataType::MULTI_FIELD: return loadMultiFieldUpdates(file, inertions, deletions);
  case DataType::FIVE_TUPLE:  return loadClassBenchUpdates(file, inertions, deletions);
  default : fclose(file); LOG_ERROR("INVALID DataType in loading updates\n");
  }

  return std::make_tuple(0, 0);
}

void
freeUpdates(int nUpdates, update_t*& updates, int type)
{
  if (!updates) return;
  for (int i = 0; i < nUpdates; ++i) {
    freeRule(updates[i].rule, type);
  }
  MEM_FREE(updates);
}

} // namespace pp
