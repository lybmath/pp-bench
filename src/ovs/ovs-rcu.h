#ifndef RCU_H
#define RCU_H

#include <stdint.h>
typedef uint32_t atomic_uint32_t;
  
#define OVSRCU_TYPE(type) type
#define ovsrcu_get(type, value) *value
#define ovsrcu_get_protected(type, value) ovsrcu_get(type, value)
#define OVSRCU_INITIALIZER(value) value
#define ovs_assert(CONDITION) ((void) (CONDITION))
#define ovsrcu_set(var, value) *var = value;
#define ovsrcu_set_hidden(var, value) ovsrcu_set(var, value)
#define ovsrcu_postpone(FUNCTION, ARG) FUNCTION(ARG);

#define atomic_read_explicit(pvalue, pvar, mode) *pvar = *pvalue;
#define atomic_thread_fence(mode)
#define atomic_read_relaxed(pvalue, pvar)        *pvar = *pvalue;
#define atomic_store_explicit(pvar, value, mode) *pvar = value;

#endif
