#ifndef RANDOM_H
#define RANDOM_H

#include <stdint.h> // for uint32_t
#include <stdlib.h> // for malloc srand, rand
#include <time.h>   // for time

uint32_t random_uint32(void)
{
  srand (time(NULL));
  return rand();
}

#endif
