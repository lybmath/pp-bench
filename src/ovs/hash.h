#ifndef HASH_H
#define HASH_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

static inline uint32_t get_unaligned_u32(const uint32_t *p_)
{
    const uint8_t *p = (const uint8_t *) p_;
    return ntohl((p[0] << 24) | (p[1] << 16) | (p[2] << 8) | p[3]);
}

/*
 * only hash_finish and mhash_finish are used directly in cmap
 */

static inline uint32_t
hash_rot(uint32_t x, int k)
{
    return (x << k) | (x >> (32 - k));
}

uint32_t hash_bytes(const void *, size_t n_bytes, uint32_t basis);
/* The hash input must be a word larger than 128 bits. */
/* void hash_bytes128(const void *_, size_t n_bytes, uint32_t basis, */
/*                    ovs_u128 *out); */

static inline uint32_t hash_int(uint32_t x, uint32_t basis);
static inline uint32_t hash_2words(uint32_t, uint32_t);
static inline uint32_t hash_uint64(const uint64_t);
static inline uint32_t hash_uint64_basis(const uint64_t x,
                                         const uint32_t basis);
uint32_t hash_3words(uint32_t, uint32_t, uint32_t);

static inline uint32_t hash_boolean(bool x, uint32_t basis);
//uint32_t hash_double(double, uint32_t basis);

static inline uint32_t hash_pointer(const void *, uint32_t basis);
static inline uint32_t hash_string(const char *, uint32_t basis);

/* Murmurhash by Austin Appleby,
 * from https://github.com/aappleby/smhasher/blob/master/src/MurmurHash3.cpp
 *
 * The upstream license there says:
 *
 * // MurmurHash3 was written by Austin Appleby, and is placed in the public
 * // domain. The author hereby disclaims copyright to this source code.
 *
 * See hash_words() for sample usage. */

static inline uint32_t mhash_add__(uint32_t hash, uint32_t data)
{
    /* zero-valued 'data' will not change the 'hash' value */
    if (!data) {
        return hash;
    }

    data *= 0xcc9e2d51;
    data = hash_rot(data, 15);
    data *= 0x1b873593;
    return hash ^ data;
}

static inline uint32_t mhash_add(uint32_t hash, uint32_t data)
{
    hash = mhash_add__(hash, data);
    hash = hash_rot(hash, 13);
    return hash * 5 + 0xe6546b64;
}

static inline uint32_t mhash_finish(uint32_t hash)
{
    hash ^= hash >> 16;
    hash *= 0x85ebca6b;
    hash ^= hash >> 13;
    hash *= 0xc2b2ae35;
    hash ^= hash >> 16;
    return hash;
}

static inline uint32_t hash_add(uint32_t hash, uint32_t data);
static inline uint32_t hash_add64(uint32_t hash, uint64_t data);

static inline uint32_t hash_add_words(uint32_t, const uint32_t *, size_t);
static inline uint32_t hash_add_words64(uint32_t, const uint64_t *, size_t);
static inline uint32_t hash_add_bytes32(uint32_t, const uint32_t *, size_t);
static inline uint32_t hash_add_bytes64(uint32_t, const uint64_t *, size_t);

#if !(defined(__SSE4_2__) && defined(__x86_64__))
/* Mhash-based implementation. */

static inline uint32_t hash_add(uint32_t hash, uint32_t data)
{
    return mhash_add(hash, data);
}

static inline uint32_t hash_add64(uint32_t hash, uint64_t data)
{
    return hash_add(hash_add(hash, data), data >> 32);
}

static inline uint32_t hash_finish(uint32_t hash, uint32_t final)
{
    return mhash_finish(hash ^ final);
}

/* Returns the hash of the 'n' 32-bit words at 'p', starting from 'basis'.
 * 'p' must be properly aligned.
 *
 * This is inlined for the compiler to have access to the 'n_words', which
 * in many cases is a constant. */
static inline uint32_t
hash_words_inline(const uint32_t *p, size_t n_words, uint32_t basis)
{
    return hash_finish(hash_add_words(basis, p, n_words), n_words * 4);
}

static inline uint32_t
hash_words64_inline(const uint64_t *p, size_t n_words, uint32_t basis)
{
    return hash_finish(hash_add_words64(basis, p, n_words), n_words * 8);
}

static inline uint32_t hash_pointer(const void *p, uint32_t basis)
{
    /* Often pointers are hashed simply by casting to integer type, but that
     * has pitfalls since the lower bits of a pointer are often all 0 for
     * alignment reasons.  It's hard to guess where the entropy really is, so
     * we give up here and just use a high-quality hash function.
     *
     * The double cast suppresses a warning on 64-bit systems about casting to
     * an integer to different size.  That's OK in this case, since most of the
     * entropy in the pointer is almost certainly in the lower 32 bits. */
    return hash_int((uint32_t) (uintptr_t) p, basis);
}

static inline uint32_t hash_2words(uint32_t x, uint32_t y)
{
    return hash_finish(hash_add(hash_add(x, 0), y), 8);
}

static inline uint32_t hash_uint64_basis(const uint64_t x,
                                         const uint32_t basis)
{
    return hash_finish(hash_add64(basis, x), 8);
}

static inline uint32_t hash_uint64(const uint64_t x)
{
    return hash_uint64_basis(x, 0);
}

#else /* __SSE4_2__ && __x86_64__ */
#include <smmintrin.h>

static inline uint32_t hash_add(uint32_t hash, uint32_t data)
{
    return _mm_crc32_u32(hash, data);
}

/* Add the halves of 'data' in the memory order. */
static inline uint32_t hash_add64(uint32_t hash, uint64_t data)
{
    return _mm_crc32_u64(hash, data);
}

static inline uint32_t hash_finish(uint64_t hash, uint64_t final)
{
    /* The finishing multiplier 0x805204f3 has been experimentally
     * derived to pass the testsuite hash tests. */
    hash = _mm_crc32_u64(hash, final) * 0x805204f3;
    return hash ^ (uint32_t)hash >> 16; /* Increase entropy in LSBs. */
}

/* Returns the hash of the 'n' 32-bit words at 'p_', starting from 'basis'.
 * We access 'p_' as a uint64_t pointer, which is fine for __SSE_4_2__.
 *
 * This is inlined for the compiler to have access to the 'n_words', which
 * in many cases is a constant. */
static inline uint32_t
hash_words_inline(const uint32_t p_[], size_t n_words, uint32_t basis)
{
    const uint64_t *p = (const void *)p_;
    uint64_t hash1 = basis;
    uint64_t hash2 = 0;
    uint64_t hash3 = n_words;
    const uint32_t *endp = (const uint32_t *)p + n_words;
    const uint64_t *limit = p + n_words / 2 - 3;

    while (p <= limit) {
        hash1 = _mm_crc32_u64(hash1, p[0]);
        hash2 = _mm_crc32_u64(hash2, p[1]);
        hash3 = _mm_crc32_u64(hash3, p[2]);
        p += 3;
    }
    switch (endp - (const uint32_t *)p) {
    case 1:
        hash1 = _mm_crc32_u32(hash1, *(const uint32_t *)&p[0]);
        break;
    case 2:
        hash1 = _mm_crc32_u64(hash1, p[0]);
        break;
    case 3:
        hash1 = _mm_crc32_u64(hash1, p[0]);
        hash2 = _mm_crc32_u32(hash2, *(const uint32_t *)&p[1]);
        break;
    case 4:
        hash1 = _mm_crc32_u64(hash1, p[0]);
        hash2 = _mm_crc32_u64(hash2, p[1]);
        break;
    case 5:
        hash1 = _mm_crc32_u64(hash1, p[0]);
        hash2 = _mm_crc32_u64(hash2, p[1]);
        hash3 = _mm_crc32_u32(hash3, *(const uint32_t *)&p[2]);
        break;
    }
    return hash_finish(hash1, hash2 << 32 | hash3);
}

/* A simpler version for 64-bit data.
 * 'n_words' is the count of 64-bit words, basis is 64 bits. */
static inline uint32_t
hash_words64_inline(const uint64_t p[], size_t n_words, uint32_t basis)
{
    uint64_t hash1 = basis;
    uint64_t hash2 = 0;
    uint64_t hash3 = n_words;
    const uint64_t *endp = p + n_words;
    const uint64_t *limit = endp - 3;

    while (p <= limit) {
        hash1 = _mm_crc32_u64(hash1, p[0]);
        hash2 = _mm_crc32_u64(hash2, p[1]);
        hash3 = _mm_crc32_u64(hash3, p[2]);
        p += 3;
    }
    switch (endp - p) {
    case 1:
        hash1 = _mm_crc32_u64(hash1, p[0]);
        break;
    case 2:
        hash1 = _mm_crc32_u64(hash1, p[0]);
        hash2 = _mm_crc32_u64(hash2, p[1]);
        break;
    }
    return hash_finish(hash1, hash2 << 32 | hash3);
}

static inline uint32_t hash_uint64_basis(const uint64_t x,
                                         const uint32_t basis)
{
    /* '23' chosen to mix bits enough for the test-hash to pass. */
    return hash_finish(hash_add64(basis, x), 23);
}

static inline uint32_t hash_uint64(const uint64_t x)
{
    return hash_uint64_basis(x, 0);
}

static inline uint32_t hash_2words(uint32_t x, uint32_t y)
{
    return hash_uint64((uint64_t)y << 32 | x);
}

static inline uint32_t hash_pointer(const void *p, uint32_t basis)
{
    return hash_uint64_basis((uint64_t) (uintptr_t) p, basis);
}
#endif

uint32_t hash_words__(const uint32_t p[], size_t n_words, uint32_t basis);
uint32_t hash_words64__(const uint64_t p[], size_t n_words, uint32_t basis);

/* Inline the larger hash functions only when 'n_words' is known to be
 * compile-time constant. */
#if __GNUC__ >= 4
static inline uint32_t
hash_words(const uint32_t p[], size_t n_words, uint32_t basis)
{
    if (__builtin_constant_p(n_words)) {
        return hash_words_inline(p, n_words, basis);
    } else {
        return hash_words__(p, n_words, basis);
    }
}

static inline uint32_t
hash_words64(const uint64_t p[], size_t n_words, uint32_t basis)
{
    if (__builtin_constant_p(n_words)) {
        return hash_words64_inline(p, n_words, basis);
    } else {
        return hash_words64__(p, n_words, basis);
    }
}

#else

static inline uint32_t
hash_words(const uint32_t p[], size_t n_words, uint32_t basis)
{
    return hash_words__(p, n_words, basis);
}

static inline uint32_t
hash_words64(const uint64_t p[], size_t n_words, uint32_t basis)
{
    return hash_words64__(p, n_words, basis);
}
#endif

static inline uint32_t
hash_bytes32(const uint32_t p[], size_t n_bytes, uint32_t basis)
{
    return hash_words(p, n_bytes / 4, basis);
}

static inline uint32_t
hash_bytes64(const uint64_t p[], size_t n_bytes, uint32_t basis)
{
    return hash_words64(p, n_bytes / 8, basis);
}

static inline uint32_t hash_string(const char *s, uint32_t basis)
{
    return hash_bytes(s, strlen(s), basis);
}

static inline uint32_t hash_int(uint32_t x, uint32_t basis)
{
    return hash_2words(x, basis);
}

/* An attempt at a useful 1-bit hash function.  Has not been analyzed for
 * quality. */
static inline uint32_t hash_boolean(bool x, uint32_t basis)
{
    const uint32_t P0 = 0xc2b73583;   /* This is hash_int(1, 0). */
    const uint32_t P1 = 0xe90f1258;   /* This is hash_int(2, 0). */
    return (x ? P0 : P1) ^ hash_rot(basis, 1);
}

/* Helper functions for calling hash_add() for several 32- or 64-bit words in a
 * buffer.  These are not hash functions by themselves, since they need
 * hash_finish() to be called, so if you are looking for a full hash function
 * see hash_words(), etc. */

static inline uint32_t
hash_add_words(uint32_t hash, const uint32_t *p, size_t n_words)
{
    for (size_t i = 0; i < n_words; i++) {
        hash = hash_add(hash, p[i]);
    }
    return hash;
}

static inline uint32_t
hash_add_words64(uint32_t hash, const uint64_t *p, size_t n_words)
{
    for (size_t i = 0; i < n_words; i++) {
        hash = hash_add64(hash, p[i]);
    }
    return hash;
}

static inline uint32_t
hash_add_bytes32(uint32_t hash, const uint32_t *p, size_t n_bytes)
{
    return hash_add_words(hash, p, n_bytes / 4);
}

static inline uint32_t
hash_add_bytes64(uint32_t hash, const uint64_t *p, size_t n_bytes)
{
    return hash_add_words64(hash, p, n_bytes / 8);
}

/* Returns the hash of 'a', 'b', and 'c'. */
uint32_t
hash_3words(uint32_t a, uint32_t b, uint32_t c)
{
    return hash_finish(hash_add(hash_add(hash_add(a, 0), b), c), 12);
}

/* Returns the hash of the 'n' bytes at 'p', starting from 'basis'. */
uint32_t
hash_bytes(const void *p_, size_t n, uint32_t basis)
{
  const uint32_t *p = (uint32_t *)p_;
    size_t orig_n = n;
    uint32_t hash;

    hash = basis;
    while (n >= 4) {
        hash = hash_add(hash, get_unaligned_u32(p));
        n -= 4;
        p += 1;
    }

    if (n) {
        uint32_t tmp = 0;

        memcpy(&tmp, p, n);
        hash = hash_add(hash, tmp);
    }

    return hash_finish(hash, orig_n);
}

/* uint32_t */
/* hash_double(double x, uint32_t basis) */
/* { */
/*     uint32_t value[2]; */
/*     BUILD_ASSERT_DECL(sizeof x == sizeof value); */

/*     memcpy(value, &x, sizeof value); */
/*     return hash_3words(value[0], value[1], basis); */
/* } */

uint32_t
hash_words__(const uint32_t p[], size_t n_words, uint32_t basis)
{
    return hash_words_inline(p, n_words, basis);
}

uint32_t
hash_words64__(const uint64_t p[], size_t n_words, uint32_t basis)
{
    return hash_words64_inline(p, n_words, basis);
}

/* #if !(defined(__x86_64__)) */
/* void */
/* hash_bytes128(const void *p_, size_t len, uint32_t basis, ovs_u128 *out) */
/* { */
/*     const uint32_t c1 = 0x239b961b; */
/*     const uint32_t c2 = 0xab0e9789; */
/*     const uint32_t c3 = 0x38b34ae5; */
/*     const uint32_t c4 = 0xa1e38b93; */
/*     const uint8_t *tail, *data = (const uint8_t *)p_; */
/*     const uint32_t *blocks = (const uint32_t *)p_; */
/*     const int nblocks = len / 16; */
/*     uint32_t h1 = basis; */
/*     uint32_t h2 = basis; */
/*     uint32_t h3 = basis; */
/*     uint32_t h4 = basis; */

/*     /\* Body *\/ */
/*     for (int i = 0; i < nblocks; i++) { */
/*         uint32_t k1 = get_unaligned_u32(&blocks[i * 4 + 0]); */
/*         uint32_t k2 = get_unaligned_u32(&blocks[i * 4 + 1]); */
/*         uint32_t k3 = get_unaligned_u32(&blocks[i * 4 + 2]); */
/*         uint32_t k4 = get_unaligned_u32(&blocks[i * 4 + 3]); */

/*         k1 *= c1; */
/*         k1 = hash_rot(k1, 15); */
/*         k1 *= c2; */
/*         h1 ^= k1; */

/*         h1 = hash_rot(h1, 19); */
/*         h1 += h2; */
/*         h1 = h1 * 5 + 0x561ccd1b; */

/*         k2 *= c2; */
/*         k2 = hash_rot(k2, 16); */
/*         k2 *= c3; */
/*         h2 ^= k2; */

/*         h2 = hash_rot(h2, 17); */
/*         h2 += h3; */
/*         h2 = h2 * 5 + 0x0bcaa747; */

/*         k3 *= c3; */
/*         k3 = hash_rot(k3, 17); */
/*         k3 *= c4; */
/*         h3 ^= k3; */

/*         h3 = hash_rot(h3, 15); */
/*         h3 += h4; */
/*         h3 = h3 * 5 + 0x96cd1c35; */

/*         k4 *= c4; */
/*         k4 = hash_rot(k4, 18); */
/*         k4 *= c1; */
/*         h4 ^= k4; */

/*         h4 = hash_rot(h4, 13); */
/*         h4 += h1; */
/*         h4 = h4 * 5 + 0x32ac3b17; */
/*     } */

/*     /\* Tail *\/ */
/*     uint32_t k1, k2, k3, k4; */
/*     k1 = k2 = k3 = k4 = 0; */
/*     tail = data + nblocks * 16; */
/*     switch (len & 15) { */
/*     case 15: */
/*         k4 ^= tail[14] << 16; */
/*         /\* fall through *\/ */
/*     case 14: */
/*         k4 ^= tail[13] << 8; */
/*         /\* fall through *\/ */
/*     case 13: */
/*         k4 ^= tail[12] << 0; */
/*         k4 *= c4; */
/*         k4 = hash_rot(k4, 18); */
/*         k4 *= c1; */
/*         h4 ^= k4; */
/*         /\* fall through *\/ */

/*     case 12: */
/*         k3 ^= tail[11] << 24; */
/*         /\* fall through *\/ */
/*     case 11: */
/*         k3 ^= tail[10] << 16; */
/*         /\* fall through *\/ */
/*     case 10: */
/*         k3 ^= tail[9] << 8; */
/*         /\* fall through *\/ */
/*     case 9: */
/*         k3 ^= tail[8] << 0; */
/*         k3 *= c3; */
/*         k3 = hash_rot(k3, 17); */
/*         k3 *= c4; */
/*         h3 ^= k3; */
/*         /\* fall through *\/ */

/*     case 8: */
/*         k2 ^= tail[7] << 24; */
/*         /\* fall through *\/ */
/*     case 7: */
/*         k2 ^= tail[6] << 16; */
/*         /\* fall through *\/ */
/*     case 6: */
/*         k2 ^= tail[5] << 8; */
/*         /\* fall through *\/ */
/*     case 5: */
/*         k2 ^= tail[4] << 0; */
/*         k2 *= c2; */
/*         k2 = hash_rot(k2, 16); */
/*         k2 *= c3; */
/*         h2 ^= k2; */
/*         /\* fall through *\/ */

/*     case 4: */
/*         k1 ^= tail[3] << 24; */
/*         /\* fall through *\/ */
/*     case 3: */
/*         k1 ^= tail[2] << 16; */
/*         /\* fall through *\/ */
/*     case 2: */
/*         k1 ^= tail[1] << 8; */
/*         /\* fall through *\/ */
/*     case 1: */
/*         k1 ^= tail[0] << 0; */
/*         k1 *= c1; */
/*         k1 = hash_rot(k1, 15); */
/*         k1 *= c2; */
/*         h1 ^= k1; */
/*     }; */

/*     /\* Finalization *\/ */
/*     h1 ^= len; */
/*     h2 ^= len; */
/*     h3 ^= len; */
/*     h4 ^= len; */

/*     h1 += h2; */
/*     h1 += h3; */
/*     h1 += h4; */
/*     h2 += h1; */
/*     h3 += h1; */
/*     h4 += h1; */

/*     h1 = mhash_finish(h1); */
/*     h2 = mhash_finish(h2); */
/*     h3 = mhash_finish(h3); */
/*     h4 = mhash_finish(h4); */

/*     h1 += h2; */
/*     h1 += h3; */
/*     h1 += h4; */
/*     h2 += h1; */
/*     h3 += h1; */
/*     h4 += h1; */

/*     out->u32[0] = h1; */
/*     out->u32[1] = h2; */
/*     out->u32[2] = h3; */
/*     out->u32[3] = h4; */
/* } */

/* #else /\* __x86_64__ *\/ */

/* static inline uint64_t */
/* hash_rot64(uint64_t x, int8_t r) */
/* { */
/*     return (x << r) | (x >> (64 - r)); */
/* } */

/* static inline uint64_t */
/* fmix64(uint64_t k) */
/* { */
/*     k ^= k >> 33; */
/*     k *= 0xff51afd7ed558ccdULL; */
/*     k ^= k >> 33; */
/*     k *= 0xc4ceb9fe1a85ec53ULL; */
/*     k ^= k >> 33; */

/*     return k; */
/* } */

/* void */
/* hash_bytes128(const void *p_, size_t len, uint32_t basis, ovs_u128 *out) */
/* { */
/*     const uint64_t c1 = 0x87c37b91114253d5ULL; */
/*     const uint64_t c2 = 0x4cf5ad432745937fULL; */
/*     const uint8_t *tail, *data = (const uint8_t *)p_; */
/*     const uint64_t *blocks = (const uint64_t *)p_; */
/*     const int nblocks = len / 16; */
/*     uint64_t h1 = basis; */
/*     uint64_t h2 = basis; */
/*     uint64_t k1, k2; */

/*     /\* Body *\/ */
/*     for (int i = 0; i < nblocks; i++) { */
/*         k1 = get_unaligned_u64(&blocks[i * 2 + 0]); */
/*         k2 = get_unaligned_u64(&blocks[i * 2 + 1]); */

/*         k1 *= c1; */
/*         k1 = hash_rot64(k1, 31); */
/*         k1 *= c2; */
/*         h1 ^= k1; */

/*         h1 = hash_rot64(h1, 27); */
/*         h1 += h2; */
/*         h1 = h1 * 5 + 0x52dce729; */

/*         k2 *= c2; */
/*         k2 = hash_rot64(k2, 33); */
/*         k2 *= c1; */
/*         h2 ^= k2; */

/*         h2 = hash_rot64(h2, 31); */
/*         h2 += h1; */
/*         h2 = h2 * 5 + 0x38495ab5; */
/*     } */

/*     /\* Tail *\/ */
/*     k1 = 0; */
/*     k2 = 0; */
/*     tail = data + nblocks * 16; */
/*     switch (len & 15) { */
/*     case 15: */
/*         k2 ^= ((uint64_t) tail[14]) << 48; */
/*         /\* fall through *\/ */
/*     case 14: */
/*         k2 ^= ((uint64_t) tail[13]) << 40; */
/*         /\* fall through *\/ */
/*     case 13: */
/*         k2 ^= ((uint64_t) tail[12]) << 32; */
/*         /\* fall through *\/ */
/*     case 12: */
/*         k2 ^= ((uint64_t) tail[11]) << 24; */
/*         /\* fall through *\/ */
/*     case 11: */
/*         k2 ^= ((uint64_t) tail[10]) << 16; */
/*         /\* fall through *\/ */
/*     case 10: */
/*         k2 ^= ((uint64_t) tail[9]) << 8; */
/*         /\* fall through *\/ */
/*     case 9: */
/*         k2 ^= ((uint64_t) tail[8]) << 0; */
/*         k2 *= c2; */
/*         k2 = hash_rot64(k2, 33); */
/*         k2 *= c1; */
/*         h2 ^= k2; */
/*         /\* fall through *\/ */
/*     case 8: */
/*         k1 ^= ((uint64_t) tail[7]) << 56; */
/*         /\* fall through *\/ */
/*     case 7: */
/*         k1 ^= ((uint64_t) tail[6]) << 48; */
/*         /\* fall through *\/ */
/*     case 6: */
/*         k1 ^= ((uint64_t) tail[5]) << 40; */
/*         /\* fall through *\/ */
/*     case 5: */
/*         k1 ^= ((uint64_t) tail[4]) << 32; */
/*         /\* fall through *\/ */
/*     case 4: */
/*         k1 ^= ((uint64_t) tail[3]) << 24; */
/*         /\* fall through *\/ */
/*     case 3: */
/*         k1 ^= ((uint64_t) tail[2]) << 16; */
/*         /\* fall through *\/ */
/*     case 2: */
/*         k1 ^= ((uint64_t) tail[1]) << 8; */
/*         /\* fall through *\/ */
/*     case 1: */
/*         k1 ^= ((uint64_t) tail[0]) << 0; */
/*         k1 *= c1; */
/*         k1 = hash_rot64(k1, 31); */
/*         k1 *= c2; */
/*         h1 ^= k1; */
/*     }; */

/*     /\* Finalization *\/ */
/*     h1 ^= len; */
/*     h2 ^= len; */
/*     h1 += h2; */
/*     h2 += h1; */
/*     h1 = fmix64(h1); */
/*     h2 = fmix64(h2); */
/*     h1 += h2; */
/*     h2 += h1; */

/*     out->u64.lo = h1; */
/*     out->u64.hi = h2; */
/* } */
/* #endif /\* __x86_64__ *\/ */

#endif
