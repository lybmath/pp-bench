/**
 * @file   ll-bitmap.hpp
 * @author Yanbiao Li <lybmath@ucla.edu>
 * @date   Sun Jul 12 17:53:53 2015
 * 
 * @brief  LLBitMap Class
 * 
 * 
 */

#ifndef __LL_BITMAP_HPP__
#define __LL_BITMAP_HPP__

#include <cstdio>
#include <cinttypes>
#include <vector>

class LLBitMap
{
public:
  static void TEST();
  
public:
  LLBitMap(const int& totalBits);
  ~LLBitMap();
  
  LLBitMap(LLBitMap&& other)
    : m_nBytes(other.m_nBytes)
    , m_bytesArray(other.m_bytesArray) {
    other.m_bytesArray = NULL;
  }
  LLBitMap& operator = (LLBitMap&& other) {
    if (this != &other) {
      m_nBytes = other.m_nBytes;
      m_bytesArray = other.m_bytesArray;
      other.m_bytesArray = NULL;
    }
    return *this;
  }
  
public:
  void
  clear();
    
  const int&
  size() const;
  
  void
  print() const;
  
  void
  print(const int& start, const int& end) const;

public: // all index starts from 0
  LLBitMap&
  setBit(const int& pos);
  
  LLBitMap&
  unsetBit(const int& pos);
  
  bool
  isBitSet(const int& pos) const;
  
  int
  countingOnes() const;/*counting all from start to end*/
  
  int
  countingOnes(const int& pos) const;/*from start to this pos (included)*/

  int
  countingOnes(const int& low, const int& high) const;/*from low to high*/

public:
  LLBitMap&
  operator |= (const LLBitMap& other);

  bool
  intersect(const LLBitMap& other);

  bool
  intersect(const int& pos);

  int
  getAllSetBits(std::vector<int>& res) const;

  void
  setAllBits();
  
protected:
  int m_nBytes;
  uint8_t* m_bytesArray;
};

#endif /*__LL_BITMAP_HPP__*/
