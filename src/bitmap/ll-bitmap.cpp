#include <ll-bitmap.hpp>
#include <common.hpp>
#include <ll-popcnt.hpp>

#ifdef __linux__
#include <string.h>
#endif

static uint8_t LLByteMask[8]  = {1,2,4,8,16,32,64,128};//1<<x
static uint8_t LLByteUMask[8] = {254,253,251,247,239,223,191,127};//255-(1<<x)
static uint8_t LLBytePMask[8] = {0,1,3,7,15,31,63,127};//(1<<x)-1
#define MAP_TOTOAL_BITS(x) ((x) * 8)
#define MAP_CHECK_POS(x) LOG_ASSERT(x < MAP_TOTOAL_BITS(m_nBytes), "out of range of bitmap. %d should < %d", pos, MAP_TOTOAL_BITS(m_nBytes));

LLBitMap::LLBitMap(const int& totalBits)
  : m_nBytes((totalBits + 7) >> 3)
  , m_bytesArray(MEM_ALLOC(m_nBytes, uint8_t))
{
  LOG_ASSERT(m_bytesArray, "allocate bytes array fail!");
  clear();
}

LLBitMap::~LLBitMap() {
  MEM_FREE(m_bytesArray);
}

void
LLBitMap::clear()
{
  memset(m_bytesArray, 0, m_nBytes);// clear this memory  
}

const int&
LLBitMap::size() const {
  return m_nBytes;
}

void
LLBitMap::print() const {
  print(0, MAP_TOTOAL_BITS(m_nBytes));
}
  
void
LLBitMap::print(const int& start, const int& end) const
{
  char msg[MAX_LOGGER_STR_LEN] = "";
  bool useColor = m_nBytes <= 128 ? true : false;
  auto totalBits = MAP_TOTOAL_BITS(m_nBytes);
  
  for (int i = 0 ; i < m_nBytes; i ++) {
    if (i > 0) sprintf(msg, "%s%s|", msg, LOG_NONE);
    uint8_t tmp = m_bytesArray[i];
    for (int j = 0 ; j < 8 ; j ++, tmp >>= 1) {
      int pos = i << 3 | j;
      if (pos >= totalBits) {
        sprintf(msg, "%s%s%c", msg, LOG_BLUE, '0' + (tmp & 0x1));
        continue;
      }
      if (pos == totalBits >> 1) {
        sprintf (msg, "%s%s|", msg, LOG_GREEN);
      }
      
      if (tmp & 0x1) {
        if (useColor && pos >= start && pos < end) {
          sprintf(msg, "%s%s1", msg, LOG_RED);
        } else {
          sprintf(msg, "%s%s1", msg, LOG_NONE);
        }
      } else {
        if (useColor && pos >= start && pos < end) {
          sprintf(msg, "%s%s0", msg, LOG_GRAY);
        } else {
          sprintf(msg, "%s%s0", msg, LOG_NONE);
        }
      }
    }
  }
  LOG_INFO("%d: {%s%s}\n", totalBits, msg, LOG_NONE);
}

LLBitMap&
LLBitMap::setBit(const int& pos) {
  MAP_CHECK_POS(pos);
  m_bytesArray[pos >> 3] |= LLByteMask[pos & 0x7];
  return *this;
}

LLBitMap&
LLBitMap::unsetBit(const int& pos) {
  MAP_CHECK_POS(pos);
  m_bytesArray[pos >> 3] &= LLByteUMask[pos & 0x7];
  return *this;
}

bool
LLBitMap::isBitSet(const int& pos) const {
  MAP_CHECK_POS(pos);
  return m_bytesArray[pos >> 3] & LLByteMask[pos & 0x7];
}

int
LLBitMap::countingOnes() const {
  return LLPopCnt(m_bytesArray, m_nBytes);
}

int
LLBitMap::countingOnes(const int& pos) const {
  MAP_CHECK_POS(pos);
  
  int in_pos = pos & 7;
  int pre_bytes = pos >> 3;

  if (in_pos ^ 7) {
    return LLPopCnt(m_bytesArray, pre_bytes) +
      LLPopCnt(m_bytesArray[pre_bytes] & LLBytePMask[in_pos + 1]);
  } else {
    return LLPopCnt(m_bytesArray, pre_bytes + 1);
  }
}

int
LLBitMap::countingOnes(const int& low, const int& high) const {
  LOG_ASSERT(low >= 0 && high <= MAP_TOTOAL_BITS(m_nBytes) && low <= high,
	     "counting rage is invalid: [%d, %d)", low, high);
  if (low == high) return 0;
  
  int lowByte  = low >> 3;// include
  int highByte = high >> 3;// exclude
  int headPos = low & 7;// include
  int tailPos = high & 7;// exclude
  int total_count = LLPopCnt(m_bytesArray + lowByte, highByte - lowByte);
  total_count -= LLPopCnt(m_bytesArray[lowByte] & LLBytePMask[headPos]);
  total_count += LLPopCnt(m_bytesArray[highByte] & LLBytePMask[tailPos]);
  return total_count;
}

LLBitMap&
LLBitMap::operator |= (const LLBitMap& other)
{
  LOG_ASSERT(m_nBytes == other.m_nBytes, "lengths are different\n");
  for (int i = m_nBytes - 1; i >= 0; -- i) {
    m_bytesArray[i] |= other.m_bytesArray[i];
  }
  return *this;
}

bool
LLBitMap::intersect(const LLBitMap& other)
{
  LOG_ASSERT(m_nBytes == other.m_nBytes, "lengths are different\n");
  bool hasSetBit = false;
  for (int i = m_nBytes - 1; i >= 0; -- i) {
    m_bytesArray[i] &= other.m_bytesArray[i];
    if (m_bytesArray[i] > 0) hasSetBit = true;
  }
  return hasSetBit;
}

bool
LLBitMap::intersect(const int& pos)
{
  MAP_CHECK_POS(pos);
  if (m_bytesArray[pos >> 3] & LLByteMask[pos & 0x7]) {
    memset(m_bytesArray, 0, m_nBytes);
    m_bytesArray[pos >> 3] |= LLByteMask[pos & 0x7];
    return true;
  }
  else {
    memset(m_bytesArray, 0, m_nBytes);
    return false;
  }
}

int
LLBitMap::getAllSetBits(std::vector<int>& res) const
{
  int nBytes = m_nBytes;
  int nOnes = 0;
  int base;
  while (nBytes --) {
    base = nBytes << 3;
    if (m_bytesArray[nBytes]) {
      for (int i = 0; i < 8; ++i) {
	if (m_bytesArray[nBytes] & (1 << i)) {
	  res[nOnes ++] = base + i;
	}
      }
    }
  }
  return nOnes;
}

void
LLBitMap::setAllBits()
{
  memset(m_bytesArray, -1, m_nBytes);
}

void
LLBitMap::TEST()
{
  /**
   * test set/unset bit. use counting ones to check
   * at most 1024 bits.
   */
  for (int nBits = 0 ; nBits <= 128 ; nBits ++) {
    LLBitMap map(nBits);
    for (int pos = 0 ; pos < nBits ; pos ++) {
      map.setBit(pos);
      LOG_ASSERT(map.countingOnes(pos, pos + 1) == 1,
		 "set bit at pos: %d fails!", pos);

      map.unsetBit(pos);
      LOG_ASSERT(map.countingOnes(pos, pos + 1) == 0,
		 "unset bit at pos: %d fails!", pos);
    }
  }
  
  LOG_INFO("TEST on set/unset PASSED!\n");

  /**
   * test counting ones.
   * at most 1024 bits, using set to help test.
   */

  for (int nBits = 0 ; nBits <= 128 ; nBits ++) {
    LLBitMap map(nBits);
 
    for (int pos = 0 ; pos < nBits ; pos ++) {
      map.setBit(pos);
      LOG_ASSERT(map.countingOnes(pos) == pos + 1,
		 "[part Z] counting with wrong result: %d = [0, %d]",
		 map.countingOnes(pos), pos);
    }
    
    int start, end;
    for (end = 0 ; end <= nBits ; end ++) {
      for (start = 0 ; start < end ; start ++) {
        LOG_ASSERT(map.countingOnes(start, end) == end - start,
		   "[part A] counting with wrong result: %d = [%d, %d)",
		   map.countingOnes(start, end), start, end);
      }
    }

    for (end = nBits ; end > 0 ; end --) {
      //printf("END BEFORE: "); map.print();
      
      if (end < nBits) {
        map.unsetBit(end);
      }

      //printf("END AFTER: "); map.print();
      for (start = 0 ; start < end ; start ++) {
        if (start > 0) {
          map.unsetBit(start - 1);
        }

        //printf("START:"); map.print(start, end);
        LOG_ASSERT(map.countingOnes(start, end) == end - start,
		   "[part B] counting with wrong result: %d = [%d, %d)",
		   map.countingOnes(start, end), start, end);
      }

      for (start = 0 ; start < end ; start ++) {
        map.setBit(start);
      }
      //printf("RECOVER: "); map.print();
    }

    int expected = nBits > 0 ? map.countingOnes(0, nBits) : 0;
    LOG_ASSERT(map.countingOnes() == expected,
	       "[part C] counting all ones is wrong: %d", nBits);
  }
  
  LOG_INFO("TEST on counting ones PASSED.\n");

  // test get all set bits
  {
    int nBits = 20;
    LLBitMap map(nBits);

    int n = 1 << nBits;
    std::vector<int> res(nBits);
    for (int i = 0; i < n; ++ i) {
      map.clear();
      for (int j = 0; j < nBits; ++ j) {
	if (i & (1 << j)) {
	  map.setBit(j);
	}
      }
      
      auto nOnes = map.getAllSetBits(res);
      for (int j = 0; j < nOnes; j ++) {
	LOG_ASSERT(i & (1 << res[j]), "invalid location: %d %d", i, res[j]);
	LOG_ASSERT(map.isBitSet(res[j]), "bit is not set: %d %d", i, res[j]);
      }
    }
  }
  
  LOG_INFO("TEST on get all set ones PASSED.\n");
  
  int nBits = 0;
  while (!globalStopRequested && (scanf("%d", &nBits), nBits)) {
    LLBitMap map(nBits);
    int loc = 0;
    LOG_INFO("please input the location to set/uset: \n");
    while (!globalStopRequested && (scanf("%d", &loc), loc)) {
      if (loc > 0) {
        map.setBit(loc - 1).print();
      } else {
        map.unsetBit(-loc - 1).print();
      }
    }
    LOG_INFO("total 1s: %d\n", map.countingOnes());

    int start, end;
    LOG_INFO("please input the two ends:\n");
    while (scanf("%d %d", &start, &end), end) {
      LOG_INFO("total 1s between [%d, %d): %d\n",
              start, end, map.countingOnes(start, end));
      map.print(start, end);
    }
  }
}
