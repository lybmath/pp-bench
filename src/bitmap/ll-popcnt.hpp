/**
 * @file   ll-popcnt.hpp
 * @author Yanbiao Li <lybmath@ucla.edu>
 * @date   Mon Jul 13 13:32:03 2015
 * 
 * @brief  provide interfaces for pop count.
 *         using the builtin instructions.
 * 
 */

#ifndef __LL_POPCNT_HPP__
#define __LL_POPCNT_HPP__

#include <stdio.h>

static unsigned char LLPopCnt8Array[256] = {
0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 
1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8
};

void _ll_pop_cnt(unsigned long& count64, const unsigned long& data) {
  count64 += __builtin_popcountl(data);
}
void _ll_pop_cnt(unsigned int& count32, const unsigned int& data) {
  count32 += __builtin_popcount(data);
}
void _ll_pop_cnt(unsigned char& count8, const unsigned char& data) {
  count8 += LLPopCnt8Array[data];
}

template<typename T>
size_t LLPopCntT(const T* buffer, const size_t& size) {
  T total_count = 0;
  for (size_t i = 0 ; i < size ; i ++) {
    _ll_pop_cnt(total_count, buffer[i]);
  }
  return total_count;
}

size_t LLPopCnt(const unsigned char& data) {
  return LLPopCnt8Array[data];
}

size_t LLPopCnt(const unsigned char* buffer, const size_t& size) {
  size_t total_count = 0;
  total_count +=
    LLPopCntT<unsigned long>((const unsigned long*)buffer, size >> 3);
  buffer += (size >> 3) << 3;
  total_count +=
    LLPopCntT<unsigned int>((const unsigned int*)buffer, (size & 7) >> 2);
  buffer += ((size & 7) >> 2) << 2;
  total_count +=
    LLPopCntT<unsigned char>(buffer, size & 3);
  return total_count;
}

size_t LLPopCntD(const unsigned char* buffer, const size_t& size) {
  size_t total_count = 0;

  //LOG_WAR("total_count: %lu", total_count);
  unsigned long* ptr64 = (unsigned long*)buffer;
  size_t num = size >> 3;
  while (num --) {
    total_count += __builtin_popcountl(*(ptr64));
    ptr64 ++;
  }

  //LOG_WAR("total_count: %lu", total_count);
  unsigned int* ptr32 = (unsigned int*)ptr64;
  unsigned int data32 = 0;
  size_t restBytes = (size & 0x7);

  switch (restBytes) {
  case 4:
    total_count += __builtin_popcount(*(ptr32));
    //LOG_WAR("4, total_count: %lu", total_count);
    break;
  case 5:case 6:case 7:
    total_count += __builtin_popcount(*(ptr32));
    restBytes &= 3;
    ptr32 ++;
  case 1:case 2:case 3:
    buffer = (unsigned char*)(ptr32);
    //LOG("%lu total_count: %lu", restBytes, total_count);
    while (restBytes --) data32 = data32 << 8 | *(buffer + restBytes);
    //LOG("data32: %u %lu",data32, __builtin_popcount(data32));
    total_count += __builtin_popcount(data32);
    //LOG("total_count: %lu", total_count);
    break;
  case 0:
    break;
  }

  return total_count;
}

#ifdef _TEST_POPCNT
void print8Array() {
  for (int i = 0 ; i < 256 ; i ++) {
    size_t count = 0;
    for (size_t num = i ; num ; count ++) num &= num - 1;
    printf("%lu, ", count);
    if (i % 16 == 15) printf("\n");
  }
}

void testPopCnt() {

  unsigned char buffer[256];
  memset(buffer, 0, 256);

  for (int i = 0 ; i < 256 ; i ++) {
    size_t count = 0;
    for (size_t num = i ; num ; count ++) num &= num - 1;
    for (int j = 1 ; j < 256 ; j ++) {
      for (int k = 0 ; k < j ; k ++) {
        //printf("%d %d %d\n", i, j, k);
        buffer[k] = i;
        ASSERT(LLPopCnt(buffer, j) == count && LLPopCntD(buffer, j) == count,
               "buffer[%d] = %d, size = %d, %llu != %llu", k, i, j, LLPopCnt(buffer, j), count);
        buffer[k] = 0;
      }
    }
  }
  LOG("Passed PopCnt test!");
}
#endif /* _TEST_POPCNT */

#endif /*__LL_POPCNT_HPP__*/

//  LocalWords:  LLPopCnt popcountl popcount testPopCnt memset
//  LocalWords:  LLPopCntT LLPopCntD
