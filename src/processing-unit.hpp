#ifndef PROCESSING_UNIT_HPP
#define PROCESSING_UNIT_HPP

#include <string>
#include <vector>
#include <packet-io.hpp>
#include <profiler-flag.hpp>

namespace pp {

typedef void* rule_t;

struct update_t
{
  uint8_t type;
  rule_t  rule;
};

enum UpdateType
{
  DELETE = 0,
  INSERT
};

class ProcessingUnit
{
public:
  static int
  generateMatchResult(const char* rule_path, const char* traffic_path, ProcessingUnit* pu);
  
public:
  ProcessingUnit(const std::string& name)
    : m_name(name)
    , m_profiler(nullptr)
  {}
  
  virtual ~ProcessingUnit() = default;
  
public:
  virtual uint64_t
  constructWithRules(int nRules, rule_t* rules) = 0;

  virtual action_t
  matchPacket(const packet_t& packet) = 0;

  virtual void
  insertRule(const rule_t& rule) {};

  virtual void
  deleteRule(const rule_t& rule) {};

public:
  struct Profiler
  {
    bool constEnabled;
    bool matchEnabled;
    bool updateEnabled;
    int nPackets;
    int bytesPerPointer;
    Profiler(int n)
      : nPackets(n)
      , constEnabled(false)
      , matchEnabled(false)
      , updateEnabled(false)
      , bytesPerPointer(4) {};
    virtual ~Profiler() = default;
    virtual void dump(FILE* file = NULL) = 0;
  };
  
  virtual Profiler*
  initializeMatchProfiler(int nPackets) { return m_profiler; }
  
public:
  std::string
  getName() const { return m_name; }

  void
  setName(const std::string& name) { m_name = name; }

  inline void
  processUpdate(const update_t& update);

protected:
  std::string m_name;
  Profiler* m_profiler;
};

void
ProcessingUnit::processUpdate(const update_t& update)
{
  switch(update.type) {
  case UpdateType::INSERT: insertRule(update.rule); break;
  case UpdateType::DELETE: deleteRule(update.rule); break;
  }
}

#define DEFINE_PROFILER					\
  virtual ProcessingUnit::Profiler*			\
  initializeMatchProfiler(int nPackets) override	\
  {							\
    return (m_profiler = new Profiler(nPackets));	\
  }

#ifdef PROFILER_ENABLED
#define INIT_PROFILER(profiler)					\
  const auto& profiler = static_cast<Profiler*>(m_profiler);
#define INIT_PROFILER_C(profiler, class)					\
  const auto& profiler = static_cast<class::Profiler*>(m_profiler);
#else
#define INIT_PROFILER(profiler)
#define INIT_PROFILER_C(profiler, class)
#endif

#ifdef PROFILER_ENABLED
#define USE_PROFILER_C(profiler, process)	\
  if (profiler && profiler->constEnabled) {	\
    process					\
  }
#else 
#define USE_PROFILER_C(profiler, process)
#endif

#ifdef PROFILER_ENABLED
#define USE_PROFILER(profiler, process)	\
  if (profiler && profiler->matchEnabled) {	\
    process					\
  }
#else 
#define USE_PROFILER(profiler, process)
#endif

#ifdef PROFILER_ENABLED
#define USE_PROFILER_U(profiler, process)	\
  if (profiler && profiler->updateEnabled) {	\
    process					\
  }
#else 
#define USE_PROFILER_U(profiler, process)
#endif
} // namespace pp

#endif // PROCESSING_UNIT_HPP
