#ifndef RULE_TRAFFIC_HPP
#define RULE_TRAFFIC_HPP

#include <packet-io.hpp>
#include <packet-classification.hpp>
#include <update-manager.hpp>
#include <vector>
#include <set>
#include <cstdio>
#include <string>
#include <tuple>
#include <functional>

struct rule_item{
  uint32_t data;
  uint8_t prefix_len;

  bool operator<(const rule_item& tmp) const
  {
	  return this->data < tmp.data;
  }
};
typedef struct rule_item rule_item;

extern std::vector< std::vector<rule_item> > rule_vec;
extern std::vector< std::vector<rule_item> > ins_vec;
extern std::set< std::vector<rule_item> > del_set;
extern std::string data_prefix;

namespace pp {

#define SAFE_OPEN_FILE(file, path)					\
  assert(path);								\
  FILE* file = fopen(path, "r");					\
  if (!file) LOG_ERROR("can not open file: %s\n", path);		

enum DataType {
  MULTI_FIELD = 0,
  FIVE_TUPLE
};

/**
 * generate packet classification rule files
*/
std::string
generatePcRuleFile(const char* filePath,
				std::string data_base_dir);

/**
 * rule generator
*/
std::string
generateRuleFile(const char* filePath, 
					int rule_nb,
					int field_nb,
					std::string data_base_dir);

/**
 * all match traffic generator
*/
std::string
generateTrafficFromVec(int tr_nb, 
						int field_nb, 
						std::string data_base_dir);

/**
 * random traffic match generator
*/
std::string
generateTrafficByRandom(int tr_nb, int field_nb, std::string data_base_dir,
			int type, const std::vector<rule_item>& rrc_vec);

/**
 * real traffic match generator
*/
std::string
generateRealTraffic(const char * real_traffic_file, 
					int number,
					std::string data_base_dir);

/**
 * create update traffic by ins_vec and del_vec 
*/
std::string
generateTrafficOfUpadate(int tr_nb, 
						int field_nb,
						std::string data_base_dir);

/**
 * create packet classification traffic from file
*/
std::string
generateTrafficOfPcFromFile(const char * filePath,
					std::string data_base_dir);

/**
 * match result generator
*/
int
generateResult(const char * rule_Path, const char * traffic_Path);

void
generateDataFile(const char* filePath, int data_nb, int field_nb,
		 std::string data_base_dir,
		 int type,
		 std::function<pp::ProcessingUnit*(void)> newPU);

/**
 * convert rules from multi fields to pc
*/
int
convertRuleM2P(const char * filePath, const char * dst_dir);

/**
 * convert traffic from multi fields to pc
*/
int
convertTrafficM2P(const char * filePath, const char * dst_dir);
						
int
loadRuleFromFile(const char* path,
		 rule_t*& rules,
		 int type = DataType::MULTI_FIELD);

int
loadTrafficFromFile(const char* path,
		    packet_t*& packets,
		    int type = DataType::MULTI_FIELD);

int
loadResultFromFile(const char* path,
		   action_t*& actions);

std::tuple<int, int>
loadUpdatesFromFile(const char* path, update_t*& inertions, update_t*& deletions,
		    int type = DataType::MULTI_FIELD);

void
freeRule(rule_t& rule, int type = DataType::MULTI_FIELD);

void
freeRules(int nRules, rule_t*& rules,
	  int type = DataType::MULTI_FIELD);

void
freePackets(int nPackets, packet_t*& packets);

void
freeActions(int nPackets, action_t*& actions);

void
freeUpdates(int nUpdates, update_t*& updates,
	    int type = DataType::MULTI_FIELD);

} // namespace pp

#endif // RULE_TRAFFIC_HPP
