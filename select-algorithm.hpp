#ifndef SELECT_ALGORITHM_HPP
#define SELECT_ALGORITHM_HPP

#include "args-parser.hpp"
#include <common.hpp>
#include <tss.hpp>
#include <tss-pr.hpp>
#include <c-tss.hpp>
#include <ec-tss.hpp>
#include <rtss.hpp>
#include <hc-tss.hpp>
#include <vector>
#include <set>

namespace pp {

  //std::vector<ProcessingUnit*>
ProcessingUnit*
selectAlgorithm(const CommandArgs& args)
{
  globalNumFields = args.nFields;

  
  if (args.algorithmName == "tss") {
    return new TupleSpaceSearch(args.nFields);
  }
  else if (args.algorithmName == "pts") {
    return new PrunedTupleSpaceSearch(args.nFields, args.treeHeight);
  }
  else if (args.algorithmName == "tc") {
    //LOG_INFO("ctss %d %d\n", args.nFields, args.threshold);
    return new ChainedTupleSpaceSearch(args.nFields, args.threshold);
  }
  else if (args.algorithmName == "htc") {
    return new HeadChainedTupleSpaceSearch(args.nFields, args.threshold);
  }
  else if (args.algorithmName == "etc") {
    if (args.maskThreshold == 0) {
      return new ExChainedTupleSpaceSearch(args.nFields, args.depth, args.threshold);
    }
    return new ExTupleChains(args.nFields, args.depth, args.threshold,
			     args.maskThreshold, args.treeHeight);
  }
  else if (args.algorithmName == "rts") {
    return new RangeTupleSpaceSearch(args.nFields);
  }

  LOG_INFO("WARNING!!!!! invalid algorithm specified, use PC instead!\n");
  return new PacketClassification("class");
}

} // namespace pp

#endif // SELECT_ALGORITHM_HPP
