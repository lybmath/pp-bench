TARGET = pp-bench
BUILD_DIR ?= build
SRC_DIRS := src
DATA_DIR := data/traffic data/rule data/update

MAKE_BUILD := $(shell mkdir -p $(BUILD_DIR))
MAKE_DATA  := $(shell mkdir -p $(DATA_DIR))
SRCS := $(shell find $(SRC_DIRS) -name "*.cpp") $(TARGET).cpp
SM_OBJS := $(SRCS:%=$(BUILD_DIR)/%.sm.o)
SM_DEPS := $(SM_OBJS:.o=.d)
EM_OBJS := $(SRCS:%=$(BUILD_DIR)/%.em.o)
EM_DEPS := $(EM_OBJS:.o=.d)
SM_INC  := -Icompile-flags/simulation
EM_INC  := -Icompile-flags/emulation

INC_DIRS  := $(shell find $(SRC_DIRS) -type d)
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

CPPFLAGS ?= $(INC_FLAGS) -MMD -MP
CC       := g++ -std=c++11 -g
SM_TGT   := $(BUILD_DIR)/$(TARGET).sm
EM_TGT   := $(BUILD_DIR)/$(TARGET).em
TARGETS  := $(SM_TGT) $(EM_TGT)

all: $(TARGETS)

test:
	@echo $(SM_OBJS)
	@echo $(EM_OBJS)

fast : CC = g++ -std=c++11 -O3
fast : $(TARGETS)

$(SM_TGT): $(SM_OBJS)
	$(CC) $(SM_OBJS) -o $@ -lpthread

$(EM_TGT): $(EM_OBJS)
	$(CC) $(EM_OBJS) -o $@ -lpthread

# c++ source
$(BUILD_DIR)/%.cpp.sm.o: %.cpp
	$(MKDIR_P) $(dir $@)
	$(CC) $(CPPFLAGS) $(SM_INC)  -c $< -o $@

$(BUILD_DIR)/%.cpp.em.o: %.cpp
	$(MKDIR_P) $(dir $@)
	$(CC) $(CPPFLAGS) $(EM_INC) -c $< -o $@


.PHONY: clean, dataclean
clean:
	$(RM) -r $(BUILD_DIR)
dataclean:
	$(RM) data/traffic/* data/rule/* data/update/*

-include $(SM_DEPS)
-include $(EM_DEPS)

MKDIR_P ?= mkdir -p
